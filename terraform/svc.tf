resource "merchant_service" "merchant_service" {
  metadata {
    name = "merchant-deployment"
  }
  spec {
    selector = {
        kubernetes_deployment.merchant_deployment.metadata.0.labels.app
    }
    port {
      port        = 80
      target_port = 8081
    }

    type = "ClusterIP"
  }
}
