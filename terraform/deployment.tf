resource "kubernetes_deployment" "merchant_deployment" {
  metadata {
    name = "merchant-deployment"
    labels = {
      app = "merchant"
    }
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "merchant"
      }
    }

    template {
      metadata {
        labels = {
          app = "merchant"
        }
      }

      spec {
        container {
          image = "mani472/merchant:1.0"
          name  = "merchant"
          port  = 8081

          env {   
           name = SHOPPING-SERVICE-URL-PORT
           value = shopping-deployment
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}
